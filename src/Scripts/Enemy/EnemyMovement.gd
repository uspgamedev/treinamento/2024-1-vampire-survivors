class_name EnemyMovement
extends Node

@onready var player: CharacterBody2D = get_tree().get_first_node_in_group("SURVIVOR")

func _physics_process(delta):
	if owner == null:
		return
	
	var _speed: int = owner.SPEED
	var _accel: int = owner.ACCEL
	
	var target: Vector2 = Vector2.ZERO
	if player != null:
		target = (player.position - owner.position).normalized()
	
	owner.velocity.x = move_toward(owner.velocity.x, _speed * target.x, _accel)
	owner.velocity.y = move_toward(owner.velocity.y, _speed * target.y, _accel)

	owner.move_and_slide()

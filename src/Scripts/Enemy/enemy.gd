class_name Enemy
extends CharacterBody2D

@export var SPEED: int = 150
@export var ACCEL: int = 100

@export var damage: int = 1

@onready var player: CharacterBody2D = Player.get_survivor()

func _ready():
	motion_mode = CharacterBody2D.MOTION_MODE_FLOATING
	$Health.connect("no_health", die)

func set_stats(_health: int, _speed: int, _scale: float, _damage: int):
	$Health.health = _health
	self.SPEED = _speed
	self.scale = Vector2(1.0, 1.0) * _scale
	self.damage = _damage

func take_damage(damage: int):
	$Health.damage(damage)

func die():
	queue_free()

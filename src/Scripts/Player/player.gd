extends CharacterBody2D
class_name Player

@onready var health: Health = $Health
@export var shader: ShaderMaterial
var tween: Tween

static var _SURVIVOR: Player = null

static func get_survivor():
	return _SURVIVOR

func _ready():
	motion_mode = CharacterBody2D.MOTION_MODE_FLOATING
	health.connect("no_health", die)
	Player._SURVIVOR = self

func _process(delta):
	handle_enemy_collisions()

func handle_enemy_collisions() -> void:
	for i in get_slide_collision_count():
		var collider: Object = get_slide_collision(i).get_collider()
		
		if collider and collider.is_in_group("ENEMY"):
			health.damage(collider.damage)
			tween = create_tween()
			tween.tween_method(func(x): shader.set_shader_parameter("entryColor", x), Color.WHITE, Color(3, 1, 1, 1), .3)
			tween.tween_method(func(x): shader.set_shader_parameter("entryColor", x), Color(2, 1, 1, 1), Color.WHITE, .3)

func die() -> void:
	queue_free()

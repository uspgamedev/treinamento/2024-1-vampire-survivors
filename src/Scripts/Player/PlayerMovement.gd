class_name PlayerMovement
extends Node

@export var speed: int = 350
@export var accel: int = 4000

func _physics_process(delta: float):
	if owner == null:
		return
	
	var direction: Vector2 = Input.get_vector("left", "right", "up", "down")
	
	owner.velocity.x = move_toward(owner.velocity.x, speed * direction.x, accel * delta)
	owner.velocity.y = move_toward(owner.velocity.y, speed * direction.y, accel * delta)
	
	owner.move_and_slide()

class_name Health
extends Node2D

@export var health: int = 100

@onready var label: Label = $Label

signal no_health

func _ready():
	_set_label_text()

func damage(amount: int) -> void:
	# Do nothing if owner is "dead"
	if health <= 0:
		return

	health -= amount
	if health <= 0:
		emit_signal("no_health")
		health = 0
		
	_set_label_text()

func _set_label_text():
	label.text = "Vida: " + str(health)

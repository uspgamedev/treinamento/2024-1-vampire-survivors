extends Area2D

var damage: int = 30

func _on_body_entered(body):
	if body.is_in_group("ENEMY"):
		body.take_damage(damage)

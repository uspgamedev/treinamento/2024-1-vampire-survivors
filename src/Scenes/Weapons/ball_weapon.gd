extends Node2D

var rotation_velocity: float = 1.5 * PI

func _physics_process(delta):
	rotation += rotation_velocity * delta
	if rotation > 2 * PI:
		rotation -= 2 * PI

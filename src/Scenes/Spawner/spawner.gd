extends Node2D

@export var ENEMY_SCENE: PackedScene
@export var enemy_resources : Array[EnemyResource]

var current_enemy: int = 0
	
func spawn_enemy():
	var enemy_to_spawn: Enemy = ENEMY_SCENE.instantiate()
	get_tree().current_scene.add_child(enemy_to_spawn)
	
	var current_resource = enemy_resources[current_enemy]
	enemy_to_spawn.set_stats(
		current_resource.health,
		current_resource.speed,
		current_resource.scale,
		current_resource.damage
	)
	enemy_to_spawn.position = self.position
	
	current_enemy = (current_enemy + 1) % enemy_resources.size()

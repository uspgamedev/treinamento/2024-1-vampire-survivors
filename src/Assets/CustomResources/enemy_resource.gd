extends Resource
class_name EnemyResource

@export var health: int
@export var damage: int
@export var speed: int
@export var scale: float
